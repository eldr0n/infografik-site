const util = require("util");
const exec = util.promisify(require("child_process").exec);

const projectPattern = /(?<=Project: )\S*/gm;
const createdPattern = /(?<=Created: ).*/gm;
const languagesPattern = /(?<=Languages: ).*\n.*\n.*/gm;
const authorsPattern = /(?<=Authors: ).*\n.*/gm;
const changePattern = /(?<=change: ).*/gm;
const repoPattern = /(?<=Repo: )\S*/gm;
const commitsPattern = /(?<=Commits: )\S*/gm;
const locPattern = /(?<=code: )\S*/gm;
const sizePattern = /(?<=Size: ).*/gm;

async function getData() {
  let parsedData;
  try {
    const { stdout } = await exec(
      "onefetch --off /home/eldr0n/Dokumente/curiensis"
    );
    // console.log("stdout:", stdout);
    parsedData = await parseData(stdout);
  } catch (err) {
    console.error(err);
  }
  return parsedData;
}

async function parseData(data) {
  let langs = [];
  let langsPerCent = [];
  let authors = [];
  data
    .match(languagesPattern)
    .toString()
    .replaceAll("(", "\n")
    .replaceAll(")", " \n")
    .split("\n")
    .forEach((e) => {
      if (e.includes("%")) {
        langsPerCent.push(e.trim());
      } else if(e!=" ") {
          langs.push(e.trim())
      }
    });
  data
    .match(authorsPattern)
    .toString()
    .split("\n")
    .forEach((e) => authors.push(e.trim()));

  return {
    project: data.match(projectPattern)[0],
    created: data.match(createdPattern)[0],
    langs,
    langsPerCent,
    authors,
    change: data.match(changePattern)[0],
    repo: data.match(repoPattern)[0],
    commits: data.match(commitsPattern)[0],
    loc: data.match(locPattern)[0],
    size: data.match(sizePattern)[0],
  };
}

module.exports = {
  getData,
};
