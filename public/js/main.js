let ctx = document.getElementById("graph");
let graph = new Chart(ctx);

let langs = [];
let langsPerCent = [];

fetch("/api/data")
// fetch("/api/static")
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    setData(data);
  });

function setData(data) {
  document.getElementById("title").innerHTML = data.project + "\n" + data.repo;
  document.getElementById("commits").innerHTML = data.commits;
  document.getElementById("loc").innerHTML = data.loc;
  document.getElementById("authors").innerHTML = data.authors.length;
  document.getElementById("size").innerHTML = data.size;
  document.getElementById("change").innerHTML = data.change.split(" ")[1];
  document.getElementById("change2").innerHTML = data.change.split(" ")[2];
  document.getElementById("created").innerHTML = data.created;

  data.langs.forEach((e) => langs.push(e));
  data.langsPerCent.forEach((e) => {
    langsPerCent.push(e.split(" ")[0]);

    bar();
  });
}
const chartData = {
  labels: langs,
  datasets: [
    {
      data: langsPerCent,
      backgroundColor: [
        "#f9de45",
        "#ef6026",
        "#007acc",
        "#4ad697",
        "#4182b8",
        "#cc0000",
        "#b266ff",
      ],
    },
  ],
};

function bar() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "bar",
    data: chartData,
    options: {
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            gridLines: { display: false },
            ticks: { fontSize: 20, fontColor: "black" },
          },
        ],
        yAxes: [
          {
            gridLines: { display: false },
            ticks: { display: false },
          },
        ],
      },
    },
  });
}

function line() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "line",
    data: chartData,
    options: {
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            gridLines: { display: false },
            ticks: { fontSize: 20, fontColor: "black" },
          },
        ],
        yAxes: [
          {
            gridLines: { display: false },
          },
        ],
      },
    },
  });
}

function horizontal() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "horizontalBar",
    data: chartData,
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [
          {
            gridLines: { display: false },
            ticks: { fontSize: 20, fontColor: "black" },
          },
        ],
        xAxes: [
          {
            gridLines: { display: false },
            ticks: { display: false },
          },
        ],
      },
    },
  });
}

function radar() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "radar",
    data: chartData,
    options: {
      legend: {
        display: false,
      },

      scales: {
        ticks: { fontSize: 20, fontColor: "black" },
      },
    },
  });
}

function pie() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "pie",
    data: chartData,
    options: {},
  });
}

function doughnut() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "doughnut",
    data: chartData,
    options: {},
  });
}

function polar() {
  graph.destroy();
  graph = new Chart(ctx, {
    type: "polarArea",
    data: chartData,
    options: {},
  });
}
