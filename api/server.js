const express = require("express");
const { getData } = require("./utils/grabber.js");
const app = express();
const port = 4000;

app.get("/api/data", async function (req, res) {
  res.send(await getData());
});

// app.get("/api/static", function (req, res) {
//   res.send({
//     project: "curiensis",
//     created: "vor 3 Monaten",
//     langs: ["JSX", "JavaScript", "Markdown", "HTML", "XML"],
//     langsPerCent: ["65.4 %", "28.6 %", "4.8 %", "0.9 %", "0.3 %"],
//     authors: ["55% mercenary99 174", "44% Rico Good 138"],
//     change: "vor 21 Stunden",
//     repo: "git@gitlab.com:eldr0n/curiensis.git",
//     commits: "312",
//     loc: "2720",
//     size: "22.49 MiB (221 files)",
//   });
// });

app.use(express.static("../public"));

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`);
});
